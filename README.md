## Omnistart Rails Template

An authoritative template for rails 6.

This was heavily inspired by [jumpstart](https://github.com/excid3/jumpstart) template

### Getting Started

#### Requirements

You'll need the following installed to run the template successfully:

- Ruby 2.5 or higher
- bundler: `gem install bundler`
- rails: `gem install rails`

#### Creating a new app

From the repository:

```
rails new replace_app_name --database=postgresql -m https://gitlab.com/omnicode.solutions/omnistart/-/raw/master/template.rb?inline=false
```

Or locally:

```
rails new replace_app_name --database=postgresql -m template.rb
```

## Author

| ![Mauricio Vieira](https://avatars2.githubusercontent.com/u/95258?s=150&v=4)|
|:---------------------:|
|  [Mauricio Vieira](https://github.com/mauriciovieira/)   |

+ <https://mauriciovieira.net>
+ <https://twitter.com/mauriciovieira>